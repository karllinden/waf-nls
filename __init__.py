#
# Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import os

from waflib import Build, Errors, Utils

def check_for_nls_function(conf, headers, function_name):
    fragment = '#include <stdio.h>\n'
    for header in headers:
        fragment += '#include <{}>\n'.format(header)
    fragment += 'int main(void) {\n'

    if function_name.endswith('gettext'):
        fragment += '    char * msg = {}('.format(function_name)

        # Construct the arguments.
        arg_codes = function_name[:-7]
        args = ['"msgid"']
        if arg_codes.startswith('d'):
            args.insert(0, '"domain"')
            arg_codes = arg_codes[1:]
        if arg_codes.endswith('n'):
            args.append('"msgid_plural"')
            args.append('42')
            arg_codes = arg_codes[:-1]
        if arg_codes.startswith('c'):
            args.append('1984')
            arg_codes = arg_codes[1:]

        # No argument codes must be left, because then this function is
        # not known.
        if arg_codes:
            raise ValueError(
                "unknown nls function {}".format(function_name))

        # Append the arguments.
        fragment += args[0]
        for x in args[1:]:
            fragment += ', ' + x
        fragment += ');\n'

        # Finally print the result to avoid an unused variable warning.
        fragment += '    puts(msg);\n'
    elif function_name == 'textdomain':
        fragment += '    textdomain("domainname");\n'
    elif function_name == 'bindtextdomain':
        fragment += '    bindtextdomain("domainname", "dirname");\n'
    elif function_name == 'setlocale':
        fragment += '    setlocale(0, "locale");\n'
    else:
        raise ValueError(
            "unknown nls function {}".format(function_name))

    fragment += '}'

    conf.check(
            msg='Checking for function ' + function_name,
            fragment=fragment,
            use='INTL',
            define_name=conf.have_define(function_name))

def check_for_nls_functions(conf):
    headers = []
    if conf.env['HAVE_LIBINTL_H']:
        headers.append('libintl.h')
    if conf.env['HAVE_LOCALE_H']:
        headers.append('locale.h')

    # The user of this module can set NLS_FUNCTIONS to check for
    # only the functions actually needed. If not set all gettext
    # functions are checked.
    fns = conf.env['NLS_FUNCTIONS'] or [
        # libintl.h
        'gettext',
        'dgettext',
        'dcgettext',
        'ngettext',
        'dngettext',
        'dcngettext',
        'textdomain',
        'bindtextdomain',

        # locale.h
        'setlocale'
    ]
    all_found = True
    for fn in fns:
        try:
            check_for_nls_function(conf, headers, fn)
        except Errors.ConfigurationError:
            all_found = False
    if not all_found:
        raise Errors.ConfigurationError

def options(opt):
    opt.load("autooptions")

    if hasattr(opt, 'nls_default'):
        default = opt.nls_default
    else:
        default = True

    nls = opt.add_auto_option(
            'nls',
            'Enable native language support',
            default=default)

    nls.check(header_name='libintl.h', mandatory=False)
    nls.check(header_name='locale.h', mandatory=False)
    nls.check(lib='intl', mandatory=False)
    nls.add_function(check_for_nls_functions)

    # build
    nls.find_program('msgfmt')

    # update-po
    nls.find_program('msgmerge')
    nls.find_program('xgettext')

def configure(conf):
    conf.load('autooptions')

def find_domain(bld):
    if 'DOMAIN' in bld.env:
        return bld.env['DOMAIN']
    elif 'PACKAGE_NAME' in bld.env:
        return bld.env['PACKAGE_NAME']
    else:
        bld.fatal('DOMAIN or PACKAGE_NAME must be defined in the '
                  'environment')

def find_podir(bld):
    return getattr(bld.env, 'PODIR') or 'po'

def find_linguas(bld):
    if not 'LINGUAS' in bld.env:
        bld.fatal('LINGUAS must be defined in the environment')
    return bld.env['LINGUAS']

def build(bld):
    if not bld.env['NLS']:
        return

    domain = find_domain(bld)
    podir = find_podir(bld)
    linguas = find_linguas(bld)

    mofile = domain + '.mo'
    for lingua in linguas:
        install_dir = os.path.join(
                bld.env['LOCALEDIR'],
                lingua,
                'LC_MESSAGES')
        bld(
            source        = os.path.join(podir, lingua + '.po'),
            target        = os.path.join(podir, lingua, mofile),
            rule          = '${MSGFMT} -o ${TGT} ${SRC}',
            install_path = install_dir
        )

def update_po(bld):
    if not bld.env['NLS']:
        return

    required_env_vars = [
        'PACKAGE_NAME',
        'PACKAGE_VERSION',
        'POTFILES'
    ]
    for x in required_env_vars:
        if not x in bld.env:
            bld.fatal('%s must be defined in the environment' % x)

    podir = find_podir(bld)
    domain = find_domain(bld)
    potfile = getattr(bld.env, 'POTFILE') or \
            os.path.join(podir, domain + '.pot')

    if 'XGETTEXT_OPTIONS' in bld.env:
        bld.env['XGETTEXT_OPTIONS'] = \
            Utils.to_list(bld.env['XGETTEXT_OPTIONS'])
    else:
        bld.env['XGETTEXT_OPTIONS'] = ['--keyword=_', '--keyword=N_']

    bld.env['XGETTEXT_OPTIONS'].append('--foreign-user')

    options = [
        'package-name',
        'package-version',
        'copyright-holder',
        'msgid-bugs-address'
    ]
    for option in options:
        varname = option.upper().replace('-', '_')
        value = bld.env[varname]
        if value:
            argument = '--%s="%s"' % (option, value)
            bld.env['XGETTEXT_OPTIONS'].append(argument)

    bld(
        source = bld.env['POTFILES'],
        target = potfile,
        rule = '${XGETTEXT} ${XGETTEXT_OPTIONS} -o ${TGT} ${SRC}'
    )

    for lingua in bld.env['LINGUAS']:
        pofile = bld.srcnode.make_node(
                os.path.join(podir, lingua + '.po'))
        bld(
            source = potfile,
            target = pofile,
            rule = '${MSGMERGE} --backup=none --update ${TGT} ${SRC}'
        )

class UpdatePoContext(Build.BuildContext):
    cmd = 'update-po'
    fun = 'update_po'
